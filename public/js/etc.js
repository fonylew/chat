function getTime() {
    var date = new Date();
    var str = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return str;
}

function getTodayDate() {
    var date = new Date();
    var str = date.getDate() + "/" +(date.getMonth()+1)+"/"+date.getFullYear() ;
    return str;
}