var express = require('express');
var app = express();
var path = require('path');
var port = process.env.PORT||3000;
require('date-utils');

//TRY DATABASE
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/chat', function(err) {
  if (err) console.log(err);
  else console.log('connected to mongodb');
});

var chatSchema = mongoose.Schema({
  username: String,
  msg: String,
  room: String,
  created: {type: Date, default: Date.now}
});

var logSchema = mongoose.Schema({
  username: String,
  room: String,
  lastConnect: {type: Date, default: Date.now}
});

var Chat = mongoose.model('Message', chatSchema);
var LastLog = mongoose.model('Lastconnect', logSchema);
//TRY DATABASE


var expressSession = require('express-session');
app.use(expressSession({secret: 'chatdissys'}));

//====== LOGIN ============
var flash = require('connect-flash')
  , passport = require('passport')
  , util = require('util')
  , LocalStrategy = require('passport-local').Strategy;

app.use(flash());

var server = app.listen(port, function() {
    console.log('Program running on port : '+ port);
});

var io = require('socket.io').listen(server);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.render('login', { user: req.user });
});

app.get('/index', function(req, res) {
    res.render('index', { user: req.session.user, rooms: rooms, message: req.flash('error') });
});
//----socket.io
io.sockets.on('connection', function(socket) {
  socket.on('adduser', function(username){
    socket.username = username;
    socket.room = 'defaultRoom';
    usernames[username] = username;
    socket.join('defaultRoom');
    var lastLog = Date.now();
    var findLog = false;
    var hasUnseen = false;
    LastLog.find({room: 'defaultRoom', username: socket.username}, function(err, docs){
      if(err) throw err;
      if (docs.length > 0) {
        lastLog = docs[docs.length-1].lastConnect;
        findLog = true;
      }
    });
    Chat.find({room: 'defaultRoom'}, function(err, docs){
      if (err) throw err;
      if (findLog == false) socket.emit('oldmsg', docs, 0);
      else {
        for (var i = 0; i < docs.length; i++) {
          if(Date.compare(docs[i].created, lastLog) == 1) {
            socket.emit('oldmsg', docs, i);
            hasUnseen = true;
            break;
          }
        }
        if (hasUnseen == false) socket.emit('oldmsg', docs, -1);
      }
    });
    socket.emit('fromserver', 'you have connected to defaultRoom','defaultRoom');
    socket.broadcast.to('defaultRoom').emit('fromserver', username + ' has connected to this room','defaultRoom');
    socket.emit('updaterooms', rooms, 'defaultRoom');
  });

  socket.on('createroom', function(roomname){
    rooms[roomname] = roomname;
  });

  socket.on('sendchat', function (data) {
      var newMsg = new Chat({msg: data, username: socket.username, room: socket.room});
      newMsg.save(function(err) {
          if(err) throw err;
          io.sockets.in(socket.room).emit('updatechat', socket.username, data, socket.room);    
      });
   });

  socket.on('switchRoom', function(newroom){
    var newLog = new LastLog({username: socket.username, room: socket.room});
    newLog.save(function(err) {
        if(err) throw err;
    });
    socket.leave(socket.room);
    socket.join(newroom);
    socket.emit('fromserver', 'you have connected to '+ newroom, newroom);
    socket.broadcast.to(socket.room).emit('fromserver', socket.username+' has left this room',socket.room);
    socket.broadcast.to(newroom).emit('fromserver', socket.username+' has joined this room',newroom);
    var lastLog = Date.now();
    var findLog = false;
    var hasUnseen = false;
    LastLog.find({room: newroom, username: socket.username}, function(err, docs){
      if(err) throw err;
      if (docs.length > 0) {
        lastLog = docs[docs.length-1].lastConnect;
        findLog = true;
      }
    });
    Chat.find({room: newroom}, function(err, docs){
      if(err) throw err;
      if (findLog == false) socket.emit('oldmsg', docs, 0);
      else {
        for (var i = 0; i < docs.length; i++) {
          if(Date.compare(docs[i].created, lastLog) == 1) {
            socket.emit('oldmsg', docs, i);
            hasUnseen = true;
            break;
          }
        }
        if (hasUnseen == false) socket.emit('oldmsg', docs, -1);
      }
    });
    socket.room = newroom;
    socket.emit('updaterooms', rooms, newroom);
  });

  socket.on('disconnect', function(){
    delete usernames[socket.username];
    io.sockets.emit('updateusers', usernames);
    socket.broadcast.emit('fromserver', socket.username + ' has disconnected',socket.room);
    var newLog = new LastLog({username: socket.username, room: socket.room});
    newLog.save(function(err) {
        if(err) throw err;
    });
    socket.leave(socket.room);
  });
});
//-------------


app.use(passport.initialize());
app.use(passport.session());

var users = [
    { id: 1, username: 'mint', password: 'pass', email: 'email1@example.com' }
  , { id: 2, username: 'korn', password: 'pass', email: 'email2@example.com' }
  , { id: 3, username: 'fon', password: 'pass', email: 'email3@example.com' }
  , { id: 4, username: 'earth', password: 'pass', email: 'email4@example.com' }
  , { id: 5, username: 'user', password: 'pass', email: 'user@example.com' }
];

function findById(id, fn) {
  var idx = id - 1;
  if (users[idx]) {
    fn(null, users[idx]);
  } else {
    fn(new Error('User ' + id + ' does not exist'));
  }
}

function findByUsername(username, fn) {
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    if (user.username === username) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}

//FOR ROOM CHAT TEST
var usernames = {};
var rooms = {defaultRoom: 'defaultRoom', room1: 'room1', room2: 'room2'}



passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  findById(id, function (err, user) {
    done(err, user);
  });
});


passport.use(new LocalStrategy(function(username, password, done) {
    process.nextTick(function () {
      findByUsername(username, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
        if (user.password != password) { return done(null, false, { message: 'Invalid password' }); }
        return done(null, user);
      })
    });
  }
));

app.get('/login', function(req, res){
  res.render('login', { user: req.user, message: req.flash('error') });
});



var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: false }), function(req, res) {
    req.session.user = req.body.username;
    res.redirect('/index');
});

app.post('/index', function(req,res) {
  res.redirect('/logout');
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});